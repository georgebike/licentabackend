from marshmallow import fields, Schema
from . import db
from . import bcrypt
from .RiderInfoModel import RiderInfoSchema
from .TrainerInfoModel import TrainerInfoSchema


class UserModel(db.Model):
    """

    User Model
    """

    __tablename__ = "user"

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)
    role = db.Column(db.String(128), nullable=False)
    trainer_info = db.relationship('TrainerInfoModel', backref='user', passive_deletes=True, lazy=True)
    rider_info = db.relationship('RiderInfoModel', backref='user', passive_deletes=True, lazy=True)
    rides = db.relationship('RideModel', backref='user', passive_deletes=True, lazy=True)

    def __init__(self, data):
        self.username = data.get('username')
        self.password = self.__generate_hash(data.get('password'))
        self.role = data.get('role')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update_password(self, data):
        for key, item in data.items():
            if key == 'password':
                self.password = self.__generate_hash(item)
                item = self.password
            setattr(self, key, item)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __generate_hash(self, password):
        return bcrypt.generate_password_hash(password, rounds=10).decode("utf-8")

    def check_hash(self, password):
        return bcrypt.check_password_hash(self.password, password)

    @staticmethod
    def get_user_by_username(username):
        return UserModel.query.filter_by(username=username).first()

    @staticmethod
    def get_user_by_id(id):
        return UserModel.query.filter_by(id=id).first()

    def __repr__(self):
        return '<id {}>'.format(self.id)


class UserSchema(Schema):
    """
    Serialization of the UserModel
    """

    id = fields.Int(dump_only=True)
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    role = fields.Str(required=True)
    trainer_info = fields.Nested(TrainerInfoSchema, many=True)
    rider_info = fields.Nested(RiderInfoSchema, many=True)

class CleanUserSchema(Schema):
    id = fields.Int(dump_only=True)
    username = fields.Str(required=True)
    role = fields.Str(required=True)
