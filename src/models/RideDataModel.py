from marshmallow import fields, Schema
from . import db


class RideDataModel(db.Model):
    """

    Ride Data Model
    """

    __tablename__ = 'ride_data'

    id = db.Column(db.Integer, primary_key=True)
    ride_id = db.Column(db.Integer, db.ForeignKey('ride.id', ondelete='CASCADE'),
                        nullable=False)  # Foreign key from users
    speed = db.Column(db.Float, nullable=False)
    cadence = db.Column(db.Float, nullable=False)
    power = db.Column(db.Float, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    timestamp = db.Column(db.DateTime)

    def __init__(self, data):
        self.ride_id = data.get('ride_id')
        self.speed = data.get('speed')
        self.cadence = data.get('cadence')
        self.power = data.get('power')
        self.latitude = data.get('latitude')
        self.longitude = data.get('longitude')
        self.timestamp = data.get('timestamp')

    '''needs methods implementation ...'''

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_ride_data(ride_id):
        # Return the entire data set for a specific ride
        return RideDataModel.query.filter_by(ride_id=ride_id).all()

    def __repr__(self):
        return '<id {}>'.format(self.id)


class RideDataSchema(Schema):
    id = fields.Int(dump_only=True)
    ride_id = fields.Int(required=True)
    speed = fields.Float(required=True)
    cadence = fields.Float(required=True)
    power = fields.Float(required=True)
    latitude = fields.Float(required=True)
    longitude = fields.Float(required=True)
    timestamp = fields.DateTime(required=True)
