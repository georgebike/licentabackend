from marshmallow import fields, Schema
from . import db
from .RideDataModel import RideDataSchema


class RideModel(db.Model):
    """
    Ride Model
    """

    __tablename__ = 'ride'

    id = db.Column(db.Integer, primary_key=True)
    rider_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'),
                         nullable=False)  # Foreign key from users
    date = db.Column(db.Date)
    ride_data = db.relationship('RideDataModel', backref='ride', passive_deletes=True, lazy='noload')

    def __init__(self, data):
        self.rider_id = data.get('rider_id')
        self.date = data.get('date')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @staticmethod
    def get_all_rides(rider_id):
        return RideModel.query.filter_by(rider_id=rider_id).all()

    @staticmethod
    def get_ride(id):
        return RideModel.query.get(id)

    def __repr__(self):
        return '<id {}>'.format(self.id)


class RideSchema(Schema):
    id = fields.Int(dump_only=True)
    rider_id = fields.Int(required=True)
    date = fields.Date(required=True)
    ride_data = fields.Nested(RideDataSchema, many=True, required=False)
