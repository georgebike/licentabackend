from marshmallow import fields, Schema
from . import db
from .TrainerToRiderModel import trainer_to_rider
from .RiderInfoModel import RiderInfoSchema


class TrainerInfoModel(db.Model):
    """

    Trainer Info Model
    """

    __tablename__ = "trainer_info"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'),
                        nullable=False)  # Foreign key from user
    first_name = db.Column(db.String(128), nullable=False)
    last_name = db.Column(db.String(128), nullable=False)
    discipline = db.Column(db.String(128), nullable=False)
    level = db.Column(db.String(128), nullable=False)
    certificate = db.Column(db.String(128), nullable=True)
    rider_info = db.relationship('RiderInfoModel', secondary=trainer_to_rider, passive_deletes=True, lazy=True)

    def __init__(self, data):
        self.user_id = data.get('user_id')
        self.first_name = data.get('first_name')
        self.last_name = data.get('last_name')
        self.discipline = data.get('discipline')
        self.level = data.get('level')
        self.certificate = data.get('certificate')
        self.role = data.get('role')

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        db.session.commit()

    @staticmethod
    def get_all():
        return TrainerInfoModel.query.all()

    @staticmethod
    def get_by_id(uid):
        return TrainerInfoModel.query.filter_by(user_id=uid).first()

    @staticmethod
    def get_connected(my_id):
        return TrainerInfoModel.query.filter(TrainerInfoModel.rider_info.any(id=my_id)).all()

    # def get_riders(self):
    #     return TrainerInfoModel.query.filter(TrainerInfoModel.rider_info.any(user_id=self.user_id)).all()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return '{} {} {}'.format(self.first_name, self.last_name, self.user_id)


class TrainerInfoSchema(Schema):
    """
    Serialization of the TrainerInfoModel
    """

    id = fields.Int(dump_only=True)
    user_id = fields.Int(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    discipline = fields.Str(required=True)
    level = fields.Str(required=True)
    certificate = fields.Str(required=False)
    # rider_info = fields.Nested(RiderInfoSchema, many=True, required=False)

class TrainerInfoSchemaRiderFree(Schema):
    """
    Serialization of the TrainerInfoModel
    """

    id = fields.Int(dump_only=True)
    user_id = fields.Int(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    discipline = fields.Str(required=True)
    level = fields.Str(required=True)
    certificate = fields.Str(required=False)
