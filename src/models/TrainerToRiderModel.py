from . import db

trainer_to_rider = db.Table('trainer_to_rider', db.Model.metadata,
                            db.Column('trainer_id', db.Integer, db.ForeignKey('trainer_info.id', ondelete='CASCADE')),
                            db.Column('rider_id', db.Integer, db.ForeignKey('rider_info.id', ondelete='CASCADE'))
                            )
