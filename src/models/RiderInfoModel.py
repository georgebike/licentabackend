from marshmallow import fields, Schema
from . import db
from .TrainerToRiderModel import trainer_to_rider


class RiderInfoModel(db.Model):
    """

    Rider Info Model
    """

    __tablename__ = "rider_info"

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='CASCADE'),
                        nullable=False)  # Foreign key from user
    first_name = db.Column(db.String(128), nullable=False)
    last_name = db.Column(db.String(128), nullable=False)
    age = db.Column(db.Integer, nullable=True)
    weight = db.Column(db.Integer, nullable=True)
    height = db.Column(db.Integer, nullable=True)
    discipline = db.Column(db.String(128), nullable=False)
    level = db.Column(db.String(128), nullable=False)
    bike = db.Column(db.String(128), nullable=True)
    topic = db.Column(db.String(128), nullable=False)
    trainer_info = db.relationship('TrainerInfoModel', secondary=trainer_to_rider, passive_deletes=True, lazy=True)

    def __init__(self, data):
        self.user_id = data.get('user_id')
        self.first_name = data.get('first_name')
        self.last_name = data.get('last_name')
        self.age = data.get('age')
        self.weight = data.get('weight')
        self.height = data.get('height')
        self.discipline = data.get('discipline')
        self.level = data.get('level')
        self.bike = data.get('bike')
        self.topic = data.get('topic')

    def update(self, data):
        for key, item in data.items():
            setattr(self, key, item)
        db.session.commit()

    @staticmethod
    def get_all():
        return RiderInfoModel.query.all()

    @staticmethod
    def get_by_id(uid):
        return RiderInfoModel.query.filter_by(user_id=uid).first()

    @staticmethod
    def get_connected(my_id):
        return RiderInfoModel.query.filter(RiderInfoModel.trainer_info.any(id=my_id)).all()

    # def get_trainers(self):
    #     # return RiderInfoModel.query.filter(RiderInfoModel.trainer_info.all())
    #     return RiderInfoModel.query.filter_by(id=self.id).first().trainer_info

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return '{} {} {}'.format(self.first_name, self.last_name, self.user_id)


class RiderInfoSchema(Schema):
    """
    Serialization of the RiderInfoModel
    """

    id = fields.Int(dump_only=True)
    user_id = fields.Int(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    age = fields.Int(required=False)
    weight = fields.Int(required=False)
    height = fields.Int(required=False)
    discipline = fields.Str(required=True)
    level = fields.Str(required=True)
    bike = fields.Str(required=False)
    topic = fields.Str(required=True)
    # trainer_info = fields.Nested('TrainerInfoSchema', many=True)


class RiderInfoSchemaTrainerFree(Schema):
    """
    Serialization of the RiderInfoModel
    """

    id = fields.Int(dump_only=True)
    user_id = fields.Int(required=True)
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    age = fields.Int(required=False)
    weight = fields.Int(required=False)
    height = fields.Int(required=False)
    discipline = fields.Str(required=True)
    level = fields.Str(required=True)
    bike = fields.Str(required=False)
    topic = fields.Str(required=True)