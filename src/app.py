from flask import Flask
from .config import app_config
from .models import db, bcrypt

from .views.UserView import user_api as user_blueprint
from .views.RideView import ride_api as ride_blueprint

from flasgger import Swagger


def create_app(env_name):
    """
    Create app
    """

    # app initiliazation
    app = Flask(__name__)
    app.config.from_object(app_config[env_name])
    app.config['SWAGGER'] = {
        'title': 'My API',
        'uiversion': 2,
        'headers': [
            ('Access-Control-Allow-Origin', 'Authorization, Content-Type')
        ]
    }

    # initializing bcrypt
    bcrypt.init_app(app)

    # initializing db
    db.init_app(app)

    app.register_blueprint(user_blueprint, url_prefix='/api/user')
    app.register_blueprint(ride_blueprint, url_prefix='/api/ride')

    swag = Swagger(app)

    return app

