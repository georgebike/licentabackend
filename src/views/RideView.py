# -*- coding: utf-8 -*-

import datetime
from flask import request, json, Response, Blueprint, g
from ..models.RideModel import RideModel, RideSchema
from ..models.RideDataModel import RideDataModel, RideDataSchema

ride_schema = RideSchema()
ride_data_schema = RideDataSchema()

from ..shared.Authentication import Auth

ride_api = Blueprint('ride', __name__)


# CREATE NEW RIDE
@ride_api.route('/', methods=['POST'])
@Auth.auth_required
@Auth.rider_required
def create():
    """
    CREATE RIDE
    Call this endpoint passing the current date at which the ride is created
    ---
    tags:
      - rides
    parameters:
      - name: api-token
        in: header
        description: The authentication key (jwt_token)
        type: string
        required: true
      - name: ride
        in: body
        type: object
        description: The current ride
        schema:
            type: object
            required:
                - date
            properties:
                date:
                    type: date
                    default: yyyy-mm-dd
    responses:
      400:
        name: error_create
        description: Error creating a new ride
        schema:
            id: error_create
            properties:
                _schema:
                    type: string
                    default: No data provided or invalid json format
                    description: No data provided or invalid json format
                generated-error:
                    type: string
                    default: Empty fields | Invalid api-token | Unauthorized user
                    description: Empty fields | Invalid credentials | Unauthorized user
      201:
        description: On successfully creating a ride
        schema:
          id: ride
          required:
            - id
            - user_id
            - date
          properties:
            id:
              type: integer
              description: The id of the ride
              default: 1
            user_id:
              type: int
              description: The id of the ride creator
              default: 1
            date:
              type: date
              description: The date at which the ride was created
              default: yyyy-mm-dd
    """
    req_data = request.get_json()
    if req_data is None:
        message = {'_schema': 'No data provided or invalid json format'}
        return custom_response(message, 400)

    req_data['rider_id'] = g.user.id
    try:
        req_data['date'] = datetime.datetime.strptime(req_data['date'], '%Y-%m-%d').date().isoformat()
    except ValueError:
        error = {'error': 'Invalid date (Error converting string date to datetime.date type)'}
        return custom_response(error, 400)

    data, error = ride_schema.load(
        req_data)  # validate and deserialize the req_data into an object defined by RideSchema's fields
    if error:
        return custom_response(error, 400)

    ride = RideModel(data)
    ride.save()
    data = ride_schema.dump(ride).data
    return custom_response(data, 201)


# GET ALL RIDES FROM A RIDER
@ride_api.route('/<int:user_id>', methods=['GET'])
@Auth.auth_required
def get_rides(user_id):
    """
        GET RIDES
        Call this endpoint passing the rider_id as url param
        ---
        tags:
          - rides
        parameters:
          - name: api-token
            in: header
            description: The authentication key (jwt_token)
            type: string
            required: true
          - name: user_id
            in: path
            type: integer
            required: true
            description: The id of the rider you want to get rides from
        responses:
          400:
            name: error
            description: Error getting rides
            schema:
                id: error
                properties:
                    generated-error:
                        type: string
                        default: Invalid api-token | Unauthorized user
                        description: Invalid credentials | Unauthorized user
          404:
            name: not_found
            description: Not found
            schema:
                id: not_found
                properties:
                    error:
                        type: string
                        description: Rides not found
                        default: Rides not found
          201:
            description: Return all the entries with ride data
            schema:
                type: array
                items:
                    type: object
                    required:
                        - id
                        - rider_id
                        - date
                    properties:
                        id:
                            type: int
                            default: 1
                        rider_id:
                            type: int
                            default: 1
                        date:
                            type: date
                            default: yyyy-mm-dd
        """
    rides = RideModel.get_all_rides(user_id)
    if not rides:
        return custom_response({'error': 'Rides not found'}, 404)

    ser_rides = ride_schema.dump(rides, many=True).data
    return custom_response(ser_rides, 200)


# GET ALL DATA FROM A RIDE
@ride_api.route('/data/<int:ride_id>', methods=['GET'])
@Auth.auth_required
def get_ride_data(ride_id):
    """
        GET RIDE DATA
        Call this endpoint passing the ride_id as url param
        ---
        tags:
          - rides
        parameters:
          - name: api-token
            in: header
            description: The authentication key (jwt_token)
            type: string
            required: true
          - name: ride_id
            in: path
            type: integer
            required: true
            description: The id of the ride you want to get data from
        responses:
          400:
            name: error
            description: Error getting ride data
            schema:
                id: error
                properties:
                    generated-error:
                        type: string
                        default: Invalid api-token | Unauthorized user
                        description: Invalid credentials | Unauthorized user
          404:
            name: error
            description: Not found
            schema:
                id: not_found
                properties:
                    error:
                        type: string
                        description: Ride data not found
                        default: Ride data not found
          201:
            description: Return all the entries with ride data
            schema:
                type: array
                items:
                    type: object
                    required:
                        - speed
                        - cadence
                        - power
                        - latitude
                        - longitude
                        - timestamp
                    properties:
                        speed:
                            type: double
                            default: 10.00
                        cadence:
                            type: double
                            default: 90.00
                        power:
                            type: double
                            default: 200.00
                        latitude:
                            type: double
                            default: 95.999999
                        longitude:
                            type: double
                            default: 95.999999
                        timestamp:
                            type: datetime
                            default: yyyy-mm-dd HH:mm:ss
    """
    ride_data = RideDataModel.get_ride_data(ride_id)
    if not ride_data:
        return custom_response({'error': 'No data found for this ride'}, 404)

    ser_ride_data = ride_data_schema.dump(ride_data, many=True).data
    return custom_response(ser_ride_data, 200)


# DELETE A RIDE AND ALL ITS DATA
@ride_api.route('/<int:ride_id>', methods=['DELETE'])
@Auth.auth_required
@Auth.rider_required
def delete_ride(ride_id):
    """
    DELETE RIDE
    Call this endpoint passing the ride_id as url param to delete the ride
    ---
    tags:
      - rides
    parameters:
      - name: api-token
        in: header
        description: The authentication key (jwt_token)
        type: string
        required: true
      - name: ride_id
        in: path
        type: integer
        required: true
        description: The id of the ride you want to get data from
    responses:
      400:
        name: error
        description: Error deleting ride
        schema:
            id: error
            properties:
                error:
                    type: string
                    default: Invalid api-token | Permission denied
                    description: Invalid credentials | Permission denied
      404:
        name: not_found
        description: Error ride not found
        schema:
            id: error
            properties:
                error:
                    type: string
                    default: Ride not found (invalid ride)
                    description: Ride not found (invalid ride)
      204:
        description: Successfully deleted the ride
        name: delete_success
        schema:
          id: deleted
          properties:
            message:
              type: string
              description: Ride deleted
              default: Ride deleted
    """
    ride = RideModel.get_ride(ride_id)
    if not ride:
        return custom_response({'error': 'Ride not found (invalid ride)'}, 404)

    data = ride_schema.dump(ride).data
    if data.get('rider_id') != g.user.id:
        return custom_response({'error': 'Permission denied'}, 400)

    ride.delete()
    return custom_response({'message': 'Ride deleted'}, 204)


def custom_response(res, status_code):
    """

    :param res:
    :param status_code:
    :return: flask.Response
    """

    return Response(
        mimetype="application/json",
        response=json.dumps(res),  # creates a string containing key: value in a json format
        status=status_code
    )


ride_path_list = [create, get_rides, get_ride_data, delete_ride]
