# -*- coding: utf-8 -*-

from flask import request, json, Response, Blueprint, g, jsonify
from ..models.UserModel import UserModel, UserSchema, CleanUserSchema
from ..models.RiderInfoModel import RiderInfoModel, RiderInfoSchema, RiderInfoSchemaTrainerFree
from ..models.TrainerInfoModel import TrainerInfoModel, TrainerInfoSchema, TrainerInfoSchemaRiderFree

from ..shared.Authentication import Auth

user_api = Blueprint('user', __name__)  # create a blueprint wrapper that we'll use for all user resources
user_schema = UserSchema()
clean_user_schema = CleanUserSchema()
rider_info_schema = RiderInfoSchema()
trainer_info_schema = TrainerInfoSchema()
trainer_info_schema_rider_free = TrainerInfoSchemaRiderFree()
rider_info_schema_trainer_free = RiderInfoSchemaTrainerFree()


# REGISTER
@user_api.route('/', methods=['POST'])
def create():
    """
    REGISTER
    Call this endpoint passing register credentials (username, password, role) and get back a jwt_token
    ---
    tags:
        - users
    post:
        description: Create new user with username, password and role (R / T)
        consumes:
            - application/json
        parameters:
            - in: body
              name: user
              description: User account credentials
              schema:
                type: object
                required:
                    - username
                    - password
                    - role
                properties:
                    username:
                        type: string
                    password:
                        type: string
                    role:
                        type: string
        responses:
            201:
                description: On register success return api-token in body as {'jwt_token'':' AUTO-GENERATED-KEY}
                schema:
                    type: string
                    properties:
                        _schema:
                            type: string
                            description: No data provided or invalid json format
            400:
                description: Problem on register
                schema:
                    type: string
                    properties:
                        _schema:
                            type: string
                            description: No data provided or invalid json format
                        generated-error:
                            type: string
                            description: Empty fields | Invalid credentials | Incorrect password
    """
    req_data = request.get_json()  # get the JSON object from the body of the request
    roles = ['R', 'T', 'A']
    role = req_data['role']
    if role not in roles:
        error = {'error': 'Please chose a valid role (R / T / A)'}
        return custom_response(error, 400)
    if req_data is None:
        message = {'_schema': 'No data provided or invalid json format'}
        return custom_response(message, 400)

    data, error = user_schema.load(
        req_data)  # validate and deserialize the req_data into an object defined by UserSchema's fields
    if error:
        return custom_response(error, 400)

    # check if the user already exists in the db
    user_in_db = UserModel.get_user_by_username(data.get('username'))
    if user_in_db:
        message = {'error': 'Username already registered, please supply a different one'}
        return custom_response(message, 400)

    user = UserModel(data)
    user.save()

    ser_data = user_schema.dump(
        user).data  # dump() = serialize the user object into a dict according to UserSchema's fields
    token = Auth.generate_token(ser_data.get('id'))

    auth_token = {'jwt_token': token}
    return custom_response(auth_token, 201)


# ADD INFO
@user_api.route('/info', methods=['POST'])
@Auth.auth_required
def add_info():
    """
        ADD INFO
        ---
        tags:
            - users
        post:
            description: Additional user info depending on the role
            consumes:
                - application/json
            parameters:
                - name: api-token
                  in: header
                  description: The authentication key (jwt_token)
                  type: string
                  required: true
                - name: trainer
                  in: body
                  description: Trainer account info
                  schema:
                    type: object
                    required:
                        - first_name
                        - last_name
                        - discipline
                        - level
                    properties:
                        first_name:
                            type: string
                        last_name:
                            type: string
                        discipline:
                            type: string
                        level:
                            type: string
                        certificate:
                            type: string
                - name: rider
                  in: body
                  description: Rider account info
                  schema:
                    type: object
                    required:
                        - first_name
                        - last_name
                        - discipline
                        - level
                        - topic
                    properties:
                        first_name:
                            type: string
                        last_name:
                            type: string
                        discipline:
                            type: string
                        level:
                            type: string
                        topic:
                            type: string
                        age:
                            type: integer
                        weight:
                            type: integer
                        height:
                            type: integer
                        bike:
                            type: string
            responses:
                201:
                    description: On trainer data added successfully send a message response
                    schema:
                        type: string
                        properties:
                            type: string
                            message: Trainer info added successfully

                400:
                    description: Problem on register
                    schema:
                        type: string
                        properties:
                            _schema:
                                type: string
                                description: No data provided or invalid json format
                            generated-error:
                                type: string
                                description: Empty fields | Invalid data
        """
    req_data = request.get_json()
    if req_data is None:
        message = {'_schema': 'No data provided or invalid json format'}
        return custom_response(message, 400)
    # Add the foreign key user_id from the current global g object
    req_data['user_id'] = g.user.id

    if g.user.role is 'R':
        # Check if info for this user already exists in the database
        rider_info_in_db = RiderInfoModel.get_by_id(g.user.id)
        if rider_info_in_db:
            message = {'error': 'Info for this user already exists'}
            return custom_response(message, 400)

        # Validate and deserialize the req_data into an object defined by RiderInfoSchema
        data, error = rider_info_schema.load(req_data)
        if error:
            return custom_response(error, 400)

        rider_info = RiderInfoModel(data)
        rider_info.save()

        return custom_response({'message': 'Rider info added successfully'}, 201)

    elif g.user.role is 'T':
        # Check if info for this user already exists in the database
        trainer_info_in_db = TrainerInfoModel.get_by_id(g.user.id)
        if trainer_info_in_db:
            message = {'error': 'Info for this user already exists'}
            return custom_response(message, 400)

        # Validate and deserialize the req_data into an object defined by RiderInfoSchema
        data, error = trainer_info_schema.load(req_data)
        if error:
            return custom_response(error, 400)

        trainer_info = TrainerInfoModel(data)
        trainer_info.save()

        return custom_response({'message': 'Trainer info added successfully'}, 201)


# LOGIN
@user_api.route('/login', methods=['POST'])
def login():
    """
        LOGIN
        Call this endpoint passing register credentials (username, password) and get back a jwt_token
        ---
        tags:
            - users
        post:
            description: Create new user with username, password and role (R / T)
            consumes:
                - application/json
            parameters:
            - in: body
              name: user
              description: User account credentials
              schema:
                type: object
                required:
                    - username
                    - password
                properties:
                    username:
                        type: string
                    password:
                        type: string
        responses:
            201:
                description: On register success return api-token in body as {'jwt_token' AUTO-GENERATED-KEY}
                schema:
                  id: response
                  properties:
                    jwt_token:
                      type: string
                      description: The auto generated jwt key for this user
                      default: AUTO-GENERATED-KEY
            400:
                name: login_error
                description: Problem on login
                schema:
                    id: login_error
                    properties:
                        _schema:
                            type: string
                            default: No data provided or invalid json format
                            description: No data provided or invalid json format
                        generated-error:
                            type: string
                            default: Empty fields | Invalid credentials | Incorrect password
                            description: Empty fields | Invalid credentials | Incorrect password
    """

    req_data = request.get_json()
    if req_data is None:
        message = {'_schema': 'No data provided or invalid json format'}
        return custom_response(message, 400)

    data, error = user_schema.load(req_data, partial=True)  # deserialize and validate the fields |
    # partial=True because we don't need all the user fields (just username and password)
    if error:
        return custom_response(error, 400)

    if not data.get('username') or not data.get('password'):
        return custom_response({'error': 'Please enter a username and a password to sign in'}, 400)

    user = UserModel.get_user_by_username(data.get('username'))

    if not user:
        return custom_response({'error': 'Invalid credentials.'}, 400)

    if not user.check_hash(data.get('password')):
        return custom_response({'error': 'Incorrect password.'}, 400)

    ser_user = clean_user_schema.dump(user).data

    token = Auth.generate_token(ser_user.get('id'))
    ser_user['token'] = token
    return custom_response(ser_user, 200)


# DELETE USER
@user_api.route('/', methods=['DELETE'])
@Auth.auth_required
def delete():
    """
        DELETE
        Call this endpoint to delete the current logged user and all of their content
        ---
        tags:
          - users
        parameters:
          - name: api-token
            in: header
            description: The authentication key (jwt_token)
            type: string
            required: true
        responses:
          404:
            name: error
            description: User not found
          204:
            name: message
            description: User deleted
        """
    user = UserModel.get_user_by_id(g.user.id)
    if not user:
        return custom_response({'error': 'User not found'}, 404)

    user.delete()  # TODO: Check if deleting the user will delete also the info and rides in cascade
    return custom_response({'message': 'User deleted'}, 204)


# GET CURRENT USER DATA
@user_api.route('/', methods=['GET'])
@Auth.auth_required
def get_current_user():
    """
    GET CURRENT
    Call this endpoint to get all the info for the current logged user
    ---
    tags:
      - users
    consumes:
      - application/json
    parameters:
      - name: api-token
        in: header
        description: The authentication key (jwt_token)
        type: string
        required: true
    responses:
      400:
        name: api-token
        description: Error with the api-token or not authorized user
      200:
        name: user_info
        description: Return all the info from the current logged user
    """

    if g.user.role is 'R':
        rider_info = RiderInfoModel.get_by_id(g.user.id)
        ser_rider_info = rider_info_schema.dump(rider_info).data
        return custom_response(ser_rider_info, 200)

    elif g.user.role is 'T':
        trainer_info = TrainerInfoModel.get_by_id(g.user.id)
        ser_trainer_info = trainer_info_schema.dump(trainer_info).data
        return custom_response(ser_trainer_info, 200)


# GET ALL USERS (all available trainers or riders depending on the role of the requester)
@user_api.route('/all', methods=['GET'])
@Auth.auth_required
def get_all():
    """
    GET ALL
    Call this endpoint to get all the relevant users depending on the one logged in (rider->trainers / trainer->riders)
    ---
    tags:
      - users
    parameters:
      - name: api-token
        in: header
        description: The authentication key (jwt_token)
        type: string
        required: true
    responses:
      400:
        name: api-token
        description: Error with the api-token or not authorized user
      200:
        name: all_users
        description: Return a list of all the users with the opposite role as the logged in user
    """
    if g.user.role is 'R':
        trainers = TrainerInfoModel.get_all()
        ser_trainers = trainer_info_schema.dump(trainers, many=True).data  # serialize trainers as a collection
        return custom_response(ser_trainers, 200)

    elif g.user.role is 'T':
        riders = RiderInfoModel.get_all()
        ser_riders = rider_info_schema.dump(riders, many=True).data  # serialize riders as a collection
        return custom_response(ser_riders, 200)


# GET CONNECTED USERS (trainer's riders or rider's trainers)
@user_api.route('/connected', methods=['GET'])
@Auth.auth_required
def get_connected():
    """
    GET CONNECTED
    Call this endpoint to get all the users that the current logged in user has connected with
    ---
    tags:
      - users
    parameters:
      - name: api-token
        in: header
        description: The authentication key (jwt_token)
        type: string
        required: true
    responses:
      400:
        name: api-token
        description: Error with the api-token or not authorized user
      200:
        name: all_connected_users
        description: Return a list of all the riders a trainer has or vice-versa
    """
    if g.user.role is 'R':
        rider_info = RiderInfoModel.get_by_id(g.user.id)
        connected_trainers = rider_info.trainer_info
        ser_trainers = trainer_info_schema_rider_free.dump(connected_trainers, many=True).data
        return custom_response(ser_trainers, 200)

    elif g.user.role is 'T':
        trainer_info = TrainerInfoModel.get_by_id(g.user.id)
        connected_riders = trainer_info.rider_info
        ser_riders = rider_info_schema_trainer_free.dump(connected_riders, many=True).data
        return custom_response(ser_riders, 200)


# UPDATE USER INFO
@user_api.route('/info', methods=['PUT'])
@Auth.auth_required
def update_info():
    """
    UPDATE USER INFO
    Call this endpoint with a json in body containing all the fields being updated
    ---
    tags:
        - users
    put:
        description: Update user info depending on the role
        consumes:
            - application/json
        parameters:
            - name: api-token
              in: header
              description: The authentication key (jwt_token)
              type: string
              required: true
            - name: trainer
              in: body
              description: Trainer account info
              schema:
                type: object
                properties:
                    first_name:
                        type: string
                    last_name:
                        type: string
                    discipline:
                        type: string
                    level:
                        type: string
                    certificate:
                        type: string
            - name: rider
              in: body
              description: Rider account info
              schema:
                type: object
                properties:
                    first_name:
                        type: string
                    last_name:
                        type: string
                    discipline:
                        type: string
                    level:
                        type: string
                    topic:
                        type: string
                    age:
                        type: integer
                    weight:
                        type: integer
                    height:
                        type: integer
                    bike:
                        type: string
        responses:
            201:
                description: On trainer/rider info updated successfully send back an object with the newly updated user
                schema:
                    type: object
                    properties:
                        first_name:
                            type: string
                        last_name:
                            type: string
                        discipline:
                            type: string
                        level:
                            type: string
                        topic:
                            type: string
                        age:
                            type: integer
                        weight:
                            type: integer
                        height:
                            type: integer
                        bike:
                            type: string
            400:
                name: error
                description: Problem on update
                schema:
                    id: error
                    type: string
                    properties:
                        _schema:
                            type: string
                            description: No data provided or invalid json format
                        generated-error:
                            type: string
                            description: Empty fields | Invalid data
    """
    req_data = request.get_json()

    if g.user.role is 'R':
        rider_info = RiderInfoModel.get_by_id(g.user.id)
        data, error = rider_info_schema.load(req_data, partial=True)  # Convert JSON to RiderInfo obj with schema fields
        if error:
            return custom_response(error, 400)
        if not rider_info:
            return custom_response({'message': 'You have no info in the database that can be updated'}, 400)
        rider_info.update(data)

        data = rider_info_schema.dump(rider_info).data
        return custom_response(data, 200)

    elif g.user.role is 'T':
        trainer_info = TrainerInfoModel.get_by_id(g.user.id)
        data, error = trainer_info_schema.load(req_data, partial=True)
        if error:
            return custom_response(error, 400)
        if not trainer_info:
            return custom_response({'message': 'You have no info in the database that can be updated'}, 400)
        trainer_info.update(data)

        data = trainer_info_schema.dump(trainer_info).data
        return custom_response(data, 200)


# BIND/UNBIND A RIDER WITH A TRAINER
@user_api.route('/bind/<int:user_id>', methods=['PUT', 'DELETE'])
@Auth.auth_required
def bind_users(user_id):
    """
    BIND/UNBIND USERS
    Call this endpoint to create or destroy a connection between a rider and a trainer
    ---
    tags:
      - users
    put:
        description: Connect a rider with a trainer
        parameters:
          - name: api-token
            in: header
            description: The authentication key (jwt_token)
            type: string
            required: true
          - name: user_id
            in: path
            type: integer
            required: true
            description: The id of the user you want to connect with
        responses:
            200:
                description: On connection created between a rider and a trainer
                schema:
                    id: message
                    properties:
                    message:
                        type: string
                        description: Successfully connected
                        default: You just connected with {first_name} {last_name}
            400:
                name: error
                description: Problem on creating connection
                schema:
                    id: error
                    type: string
                    properties:
                        error:
                            type: string
                            description: The selected user is not a trainer
                            default: The selected user is not a trainer
                        generated-error:
                            type: string
                            description: Invalid token | Unauthorized user
                            default: Invalid token | Unauthorized user
    delete:
        description: Destroy a rider - trainer connection
        parameters:
          - name: api-token
            in: header
            description: The authentication key (jwt_token)
            type: string
            required: true
          - name: user_id
            in: path
            type: integer
            required: true
            description: The id of the user you want to disconnect with
        responses:
            200:
                description: On connection destroyed between a rider and a trainer
                schema:
                    id: message
                    properties:
                    message:
                        type: string
                        description: Successfully disconnected
                        default: You just disconnected with {first_name} {last_name}
            400:
                name: error
                description: Problem on destroying connection
                schema:
                    id: error
                    type: string
                    properties:
                        error:
                            type: string
                            description: The selected user is not a trainer
                            default: The selected user is not a trainer
                        generated-error:
                            type: string
                            description: Invalid token | Unauthorized user
                            default: Invalid token | Unauthorized user

    """
    if g.user.role is 'R':
        rider_info = RiderInfoModel.get_by_id(g.user.id)
        trainer_info = TrainerInfoModel.get_by_id(user_id)
        message = None
        if not trainer_info:
            error = {'error': 'The selected user is not a trainer'}
            return custom_response(error, 400)
        if request.method == 'PUT':
            rider_info.trainer_info.append(trainer_info)
            message = {'message': 'You just connected with {} {}'.format(trainer_info.first_name,
                                                                         trainer_info.last_name)}
        elif request.method == 'DELETE':
            try:
                rider_info.trainer_info.remove(trainer_info)
                message = {'message': 'You just disconnected with {} {}'.format(trainer_info.first_name,
                                                                                trainer_info.last_name)}
            except ValueError:
                message = {'message': 'You already disconnected with {} {}'.format(trainer_info.first_name,
                                                                                   trainer_info.last_name)}
        rider_info.save()
        return custom_response(message, 200)
    if g.user.role is 'T':
        trainer_info = TrainerInfoModel.get_by_id(g.user.id)
        rider_info = RiderInfoModel.get_by_id(user_id)
        message = None
        if not rider_info:
            error = {'error': 'The selected user is not a rider'}
            return custom_response(error, 400)
        if request.method == 'PUT':
            trainer_info.rider_info.append(rider_info)
            message = {'message': 'You just connected with {} {}'.format(rider_info.first_name,
                                                                         rider_info.last_name)}
        elif request.method == 'DELETE':
            try:
                trainer_info.rider_info.remove(rider_info)
                message = {'message': 'You just disconnected with {} {}'.format(rider_info.first_name,
                                                                                rider_info.last_name)}
            except ValueError:
                message = {'message': 'You already disconnected with {} {}'.format(rider_info.first_name,
                                                                                   rider_info.last_name)}
        trainer_info.save()
        return custom_response(message, 200)


def custom_response(res, status_code):
    """

    :param res:
    :param status_code:
    :return: flask.Response
    """

    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        # creates a string containing key: value in a json format
        status=status_code
    )
