#!/usr/bin/env python
import threading
import time
import datetime
import paho.mqtt.client as paho
import json
import psycopg2
from psycopg2.extensions import AsIs
from urllib.request import urlopen
import urllib.error


class RideDataPoster(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.init_client()

    def run(self):
        while True:
            time.sleep(1)
            pass

    def init_client(self):
        client = paho.Client(transport='websockets')
        client.on_connect = self.on_connect
        client.on_message = self.on_message
        client.username_pw_set('george', 'Georgebike96')
        client.connect('connectedbike.cf', int('8083'))
        client.loop_start()

    def on_connect(self, client, userdata, flags, rc):
        """ Mqtt Callback - subscribe to receive ride data """
        with open("/home/pi/Programming/licentabackend/logs/mqtt/logging.txt", "a+") as f:
            f.write("{} - {}\n".format(datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "connected"))
        client.subscribe("connectedbike/ridedata/#", qos=0)

    def on_message(self, client, userdata, message):
        payload_json = json.loads(message.payload.decode())
        response = self.add_data(payload_json)
        if response is not None:
            with open("/home/pi/Programming/licentabackend/logs/mqtt/logging.txt", "a+") as f:
                f.write("{} - {}\n".format(datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), response))

    def add_data(self, ride_data):
        return_message = None
        connection, cursor = None, None
        try:
            connection = psycopg2.connect(user='george', password='Georgebike96',
                                          host='localhost', port='5432', database='db_licenta_deployment')

            columns = ride_data.keys()
            values = ride_data.values()
            insert_statement = 'INSERT INTO ride_data (%s) VALUES %s'

            cursor = connection.cursor()
            cursor.execute(insert_statement, (AsIs(','.join(columns)), tuple(values)))

            connection.commit()

        except (Exception, psycopg2.Error) as error:
            return_message = error

        finally:
            # close db connection
            if connection:
                cursor.close()
                connection.close()
            return return_message


def wait_internet_connection():
    """ This method will hang until an internet connection is established """
    while True:
        try:
            response = urlopen('https://connectedbike.cf/apidocs/', timeout=10)
            return
        except urllib.error.URLError:
            print("waiting for internet connection")
            pass


def main():
    with open("/home/pi/Programming/licentabackend/logs/mqtt/logging.txt", "a+") as f:
        f.write("{} - {}\n".format(datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S'), "CALLED MAIN"))
    # Wait for internet connection
    wait_internet_connection()

    # initialize Mqtt Listener for incoming ride data
    rideDataPoster = RideDataPoster()
    rideDataPoster.start()


if __name__ == '__main__':
    main()
